package com.yunyao.config;

import io.minio.MinioClient;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @version 3.0
 * @author: yunyi
 * @create: 2024-08-24 13:10
 * @Description:
 */
@Configuration
public class MinIoConfig {

    @Resource
    private MinIoInfo minIoInfo;

    @Bean
    public MinioClient minioClient(){
        return MinioClient.builder()
                .endpoint(minIoInfo.getEndpoint())
                .credentials(minIoInfo.getAccessKey(), minIoInfo.getSecretKey())
                .build();
    }
}
