package com.yunyao.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @version 3.0
 * @author: yunyi
 * @create: 2024-08-24 11:48
 * @Description:
 */
@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinIoInfo {
    private String endpoint;
    private String accessKey;
    private String secretKey;
    private String bucketName;
}
