package com.yunyao.controller;

import com.yunyao.utils.MinioUtil;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @version 3.0
 * @author: yunyi
 * @create: 2024-08-24 12:20
 * @Description:
 */
@Slf4j
@RestController
public class TestController {

//    @Resource
//    private MinioUtil minioUtil;
//
//    @CrossOrigin
//    @PostMapping("/api/upload")
//    public String upload(MultipartFile file) throws Exception {
//        String url = minioUtil.uploadFile(file);
//        log.info("上传成功，url:{}", url);
//        return url;
//    }
}
