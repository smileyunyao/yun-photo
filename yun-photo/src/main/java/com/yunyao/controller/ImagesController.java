package com.yunyao.controller;

import com.yunyao.entity.Images;
import com.yunyao.entity.vo.ImageVo;
import com.yunyao.result.RestBean;
import com.yunyao.service.ImagesService;
import com.yunyao.utils.MinioUtil;
import jakarta.annotation.Resource;
import jakarta.websocket.server.PathParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

/**
 * @version 3.0
 * @author: yunyi
 * @create: 2024-08-24 13:53
 * @Description:
 */
@Slf4j
@RestController
public class ImagesController {
    @Resource
    private MinioUtil minioUtil;

    @Resource
    private ImagesService imagesService;

    @PostMapping("/api/upload")
    public RestBean<?> upload(ImageVo vo) throws Exception {
        String url = minioUtil.uploadFile(vo.getImageUrl());
        log.info("上传成功，url:{}", url);
        imagesService.uploadImages(vo,url);
        log.info("type:{},tip:{},url:{}", vo.getImageType(),vo.getImageTip(),url);
        return RestBean.success();
    }
}
