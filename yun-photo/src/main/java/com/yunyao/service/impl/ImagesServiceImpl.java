package com.yunyao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunyao.entity.Images;
import com.yunyao.entity.vo.ImageVo;
import com.yunyao.service.ImagesService;
import com.yunyao.mapper.ImagesMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
* @author YunYao
* @description 针对表【images】的数据库操作Service实现
* @createDate 2024-08-24 19:00:57
*/
@Service
public class ImagesServiceImpl extends ServiceImpl<ImagesMapper, Images>
    implements ImagesService{

    @Resource

    private ImagesMapper imagesMapper;
    @Override
    public void uploadImages(ImageVo vo, String path) {
        Images images = new Images();
        String uuid = UUID.randomUUID().toString().replace("-","");
        images.setImageId(uuid);
        images.setImageType(vo.getImageType());
        images.setImageUrl(path);
        images.setImageTip(vo.getImageTip());
        images.setUpdateTime(new Date());
        images.setUploadTime(new Date());
        imagesMapper.insert(images);
    }
}




