package com.yunyao.service;

import com.yunyao.entity.Images;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yunyao.entity.vo.ImageVo;

/**
* @author YunYao
* @description 针对表【images】的数据库操作Service
* @createDate 2024-08-24 19:00:57
*/
public interface ImagesService extends IService<Images> {
            void uploadImages(ImageVo vo, String path);
}
