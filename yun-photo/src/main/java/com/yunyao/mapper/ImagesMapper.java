package com.yunyao.mapper;

import com.yunyao.entity.Images;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author YunYao
* @description 针对表【images】的数据库操作Mapper
* @createDate 2024-08-24 19:00:57
* @Entity com.yunyao.entity.Images
*/
public interface ImagesMapper extends BaseMapper<Images> {

}




