package com.yunyao.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/**
 * @version 3.0
 * @author: yunyi
 * @create: 2024-08-24 19:40
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImageVo {
    private MultipartFile imageUrl;
    private String imageType;
    private String imageTip;
}
