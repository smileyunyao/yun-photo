package com.yunyao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.yunyao.mapper")
@SpringBootApplication
public class YunPhotoApplication {

	public static void main(String[] args) {
		SpringApplication.run(YunPhotoApplication.class, args);
	}

}
