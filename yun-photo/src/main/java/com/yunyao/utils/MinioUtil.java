package com.yunyao.utils;

import com.yunyao.config.MinIoInfo;
import io.minio.*;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

import java.util.UUID;

/**
 * @version 3.0
 * @author: yunyi
 * @create: 2024-08-24 11:38
 * @Description:
 */
@Configuration
public class MinioUtil {
    @Resource
    private MinioClient minioClient;

    @Resource
    private MinIoInfo info;

    public String  uploadFile(MultipartFile file) throws Exception {
        String fileName = file.getOriginalFilename();
        String NewFileName = "";
        //判断文件名是否为空
        if (fileName != null ){
            NewFileName = UUID.randomUUID()
                    + fileName.substring(fileName.lastIndexOf("."));
        }else {
            return "文件为空";
        }
        //判断桶是否存在
        boolean bucketExists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(info.getBucketName()).build());
        if (!bucketExists){
            //如果不存在，就创建桶
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(info.getBucketName()).build());
            String policyJsonString = "{\"Version\" : \"2012-10-17\",\"Statement\":[{\"Sid\":\"PublicRead\",\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"*\"},\"Action\":[\"s3:GetObject\"],\"Resource\":[\"arn:aws:s3:::" + info.getBucketName() + "/*\"]}]}";
            //创建存储桶的时候，设置该存储桶里面的文件的访问策略，运行公开的读；
            minioClient.setBucketPolicy(SetBucketPolicyArgs.builder()
                    .bucket(info.getBucketName())
                    .config(policyJsonString)//json串，里面是访问策略
                    .build());
        }

        InputStream in = file.getInputStream();
        minioClient.putObject(PutObjectArgs.builder()
                .bucket(info.getBucketName())
                .object(NewFileName)
                .stream(in, file.getSize(), -1)
                .contentType(file.getContentType())
                .build());
        return info.getEndpoint() + "/" + info.getBucketName() + "/" + NewFileName;
    }

}
