import axios from "axios";
const http  = axios.create({
    baseURL: "http://localhost:8080",
    timeout: 5000
})

http.interceptors.request.use(function(config){
    console.log(1,config);
    return config;
},function(error){
    return Promise.reject(error);
});
http.interceptors.response.use(function(response){
    console.log(2,response);
    return response.data;
},function(error){
    return Promise.reject(error);
});
export default http;