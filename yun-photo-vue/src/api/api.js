import request from '@/utils/request'
export function getImage(){
    return request({
        url:'/api/wallpaper/views',
        method:'get',
        params:{
            type: "json"
        }
    })
}
export function uploadImage(data){
    return request({
        url:'/api/upload',
        method:'post',
        data
    })
}